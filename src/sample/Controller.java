package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
    public Label monitor;
    public Button btnOne;
    public Button btnTwo;
    public Button btnThree;
    public int a = 0, b = 0;


    public void doClick(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String number = monitor.getText();
        monitor.setText(number.concat(button.getText()));
    }

    public void doCompute(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()) {
            case "+":
                a = Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "=":
                b = Integer.parseInt(monitor.getText());
                monitor.setText(String.valueOf(a + b));
                break;
        }
    }
}
